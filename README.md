# Terraform-AWS

test laboral bice-vida

Contiene los pasos para levantar un módulo de beanstalk y la generacion de un recurso VPC en AWS

Primer paso : instalar fuente de terraform según sistema operativo


terraform init  #instala los modulos que se usarán

terraform validate #valida forma 

terraform plan infrastructure #simula un plan de como seria el deploy de todos los modulos

terraform apply -auto-approve #crea los recursos

---

terraform destroy -auto-approve #elimina los recursos

---

Aplicacion Remota

terraform apply gitlab.com/pes4mastermilan/terraform-aws/beanstalk