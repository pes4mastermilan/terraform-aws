#provider
variable "project" {}

variable "region" {}

variable "zone" {}

variable "bice_environment" {
  description = "Nombre del ambiente staging|prod|etc"
  type = string
}
