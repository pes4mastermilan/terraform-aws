resource "bice_solution_stack_name" "bvtest" {
  name        = "bice_name_application"
  description = "test-bice-vida"

  appversion_lifecycle {
    service_role          = aws_iam_role.beanstalk_service.arn
    max_count             = 128
    delete_source_from_s3 = true
  }
}

resource "aws_elastic_beanstalk_environment" "bice_environment" {
  name                = "bice_name_application"
  application         =  bice_name_application.bvtest.name
  solution_stack_name = "bice_solution_stack_name"
  
    setting {
      namespace = "aws:ec2:vpc"
      name      = "bice_vpc_id"
     value     = "vpc-xxxxxxxx"
    }

}