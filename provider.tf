provider "aws" {
  region     = "us-west-1"
  access_key = "my-access-key"
  secret_key = "my-secret-key"
}